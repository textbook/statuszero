#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""Scripts for https://thepihut.com/products/status-board-zero."""

import logging
import logging.handlers
import signal
from os import path
from typing import Callable, Generator

from safetee import safetee
from sources import R6Stats, Source, Status, Stock, Weather

FIFTEEN_MINUTES = 15 * 60

HERE = path.abspath(path.dirname(__file__))

LOGGER = logging.getLogger(__name__)


def _status_is(expected: Status) -> Callable[[Status], bool]:
    def _wrapped(status: Status) -> bool:
        return status == expected
    return _wrapped


def _iterate_forever(source: Source) -> Generator[Status, None, None]:
    while True:
        try:
            yield from source
        except Exception:  # pylint: disable=broad-except
            LOGGER.exception('Unexpected error')
            yield Status.OFF


def _setup_status(status: Status, source: Source):
    green, red = safetee(_iterate_forever(source))
    status.green.source = map(_status_is(Status.GREEN), green)
    status.green.source_delay = FIFTEEN_MINUTES
    status.red.source = map(_status_is(Status.RED), red)
    status.red.source_delay = FIFTEEN_MINUTES


def _configure_logging():
    root_logger = logging.getLogger("")
    root_logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
    handler = logging.handlers.RotatingFileHandler(path.join(HERE,
                                                             'statuszero.out'),
                                                   maxBytes=2000000,
                                                   backupCount=2)
    handler.setFormatter(formatter)
    root_logger.addHandler(handler)
    root_logger.addHandler(logging.StreamHandler())


if __name__ == '__main__':
    _configure_logging()

    from gpiozero import StatusZero

    STATUS_ZERO = StatusZero()

    TOP, MIDDLE, BOTTOM = STATUS_ZERO
    _setup_status(TOP, Stock(ticker='VMW'))
    _setup_status(MIDDLE, Weather.from_file(country='GB', city='London'))
    _setup_status(BOTTOM, R6Stats(user_id='8ac65307-274a-4246-a241-5d12ed26a462', threshold=1.0))

    signal.pause()
