import logging
from json import load
from os import getenv, path

from .source import Source, Status

LOGGER = logging.getLogger(__name__)


class Weather(Source):
    """Whether it's going to rain in the specified city."""

    def __init__(self, *, city_id: int, **kwargs):
        super().__init__(**kwargs)
        self.city_id = city_id
        self._params = dict(APPID=getenv("OPEN_WEATHER_API_TOKEN"), id=city_id)
        LOGGER.debug("Tracking weather for %d", city_id)

    def __next__(self) -> Status:
        data = self.client.get_json(
            "http://api.openweathermap.org/data/2.5/forecast",
            self._params
        )
        if data is None:
            return Status.OFF
        will_rain = [period.get("rain") for period in data.get("list", [])[:3]]
        LOGGER.debug("forecast: %r", will_rain)
        return Status.RED if any(will_rain) else Status.GREEN

    def __repr__(self):
        return f"Weather(city_id={self.city_id!r})"

    @classmethod
    def from_file(cls, country: str, city: str, **kwargs):
        """Create an instance with data from cities.json."""
        with open(
            path.join(path.abspath(path.dirname(__file__)), "cities.json"),
            encoding="utf8"
        ) as file:
            cities = load(file)
        return cls(city_id=cities[country][city], **kwargs)
