import logging
from abc import ABC, abstractmethod
from json import JSONDecodeError
from typing import Any, Optional

import requests

LOGGER = logging.getLogger(__name__)


class Client(ABC):
    """Interface for fetching data."""

    @abstractmethod
    def get_json(self, url: str, params: dict) -> Optional[Any]:
        """Get the JSON from the specified URL with query parameters."""

    @abstractmethod
    def get_text(self, url: str) -> Optional[str]:
        """Get the text from a specified URL."""


class RequestsClient(Client):
    """Implementation for making HTTP requests with requests."""

    def get_json(self, url: str, params: dict) -> Optional[Any]:
        """Get the JSON from the specified URL with query parameters."""
        if (response := self._get_data(url, params)) is None:
            return None
        try:
            return response.json()
        except JSONDecodeError:
            LOGGER.exception("Parsing error")
            return None

    def get_text(self, url: str) -> Optional[str]:
        """Get the text from a specified URL."""
        if (response := self._get_data(url, {})) is None:
            return None
        return response.text

    @staticmethod
    def _get_data(url: str, params: dict) -> Optional[requests.Response]:
        try:
            response = requests.get(url, params=params, timeout=10)
        except requests.ConnectionError:
            LOGGER.exception("Connection error")
            return None
        if response.status_code != 200:
            LOGGER.error("Received %r from %r", response.status_code, url)
            return None
        return response
