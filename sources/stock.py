import logging
from os import getenv
from typing import Optional

from .source import Source, Status

LOGGER = logging.getLogger(__name__)


class Stock(Source):
    """Whether the specified ticker is up."""

    def __init__(self, *, ticker: str, **kwargs):
        super().__init__(**kwargs)
        self.ticker = ticker
        self._params = dict(
            apikey=getenv("ALPHA_VANTAGE_API_TOKEN"),
            function="TIME_SERIES_DAILY",
            symbol=ticker,
        )
        LOGGER.debug("Tracking stock price for %s", ticker)

    def __next__(self) -> Status:
        json = self.client.get_json("https://www.alphavantage.co/query", self._params)
        if json is None:
            return Status.OFF
        if (data := self._extract_data(json)) is None:
            return Status.OFF
        LOGGER.debug("time series data: %r", data)
        try:
            is_up = float(data["4. close"]) > float(data["1. open"])
        except (KeyError, TypeError, ValueError):
            LOGGER.exception("Parsing error")
            return Status.OFF
        return Status.GREEN if is_up else Status.RED

    def __repr__(self):
        return f"Stock(ticker={self.ticker!r})"

    @staticmethod
    def _extract_data(json: dict) -> Optional[dict]:
        if (last_refreshed := json.get("Meta Data", {}).get("3. Last Refreshed")) is None:
            return None
        date_key = last_refreshed.split()[0]
        return json.get("Time Series (Daily)", {}).get(date_key)
