from abc import ABC
from collections.abc import Iterator
from enum import Enum

from .client import Client, RequestsClient


class Status(Enum):
    """The status to display."""
    GREEN = 1
    RED = 2
    OFF = 3


class Source(Iterator[Status], ABC):  # pylint: disable=too-few-public-methods
    """Defines a source of statuses for display."""

    def __init__(self, *, client: Client = RequestsClient()):
        self.client = client
