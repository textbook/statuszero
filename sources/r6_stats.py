"""Whether your Rainbow 6 Siege K/D is above the threshold."""
import logging
from typing import Optional

from bs4 import BeautifulSoup

from .source import Source, Status

LOGGER = logging.getLogger(__name__)


class R6Stats(Source):
    """Whether your Rainbow 6 Siege K/D is above the threshold."""

    def __init__(self, *, user_id: str, threshold: float, **kwargs):
        super().__init__(**kwargs)
        self.user_id = user_id
        self.threshold = threshold
        LOGGER.debug("Tracking K/D for %s with %.2f threshold", user_id, threshold)

    def __next__(self) -> Status:
        if (page := self.client.get_text(f"https://r6stats.com/stats/{self.user_id}")) is None:
            return Status.OFF
        if (value := self._extract_value_from_html(page)) is None:
            return Status.OFF
        return Status.RED if value < self.threshold else Status.GREEN

    def __repr__(self):
        return f"R6Stats(user_id={self.user_id!r}, threshold={self.threshold!r})"

    @staticmethod
    def _extract_value_from_html(page: str) -> Optional[float]:
        soup = BeautifulSoup(page, "html.parser")
        try:
            casual = soup.find("h1", string="Casual Stats").parent.parent
            stat = casual.find(
                "div",
                string="K/D Ratio",
                **{"class": "stats-card-item__label"}
            ).parent
            LOGGER.debug("stat: %s", stat)
            return float(stat.find("div", **{"class": "stats-card-item__value"}).string)
        except (AttributeError, ValueError):
            LOGGER.exception("Parsing error")
            return None
