# pylint: disable=missing-docstring

from textwrap import dedent
from unittest import mock, TestCase
from urllib.parse import parse_qs, urlparse

from requests import ConnectionError  # pylint: disable=redefined-builtin
from responses import GET, RequestsMock  # pylint: disable=no-name-in-module

from sources import R6Stats, Status, Stock, Weather


class BaseTestCase(TestCase):

    def setUp(self):
        super().setUp()
        self.subject = None
        self.url = None

    def assertContainsQuery(self, url, key, expected):  # pylint: disable=invalid-name
        query = urlparse(url).query
        params = parse_qs(query)
        if key not in params:
            raise AssertionError(f'query {key!r} not found in {query!r}')
        self.assertIn(expected, params[key])


class ContractMixin:

    def test_makes_request(self):
        with RequestsMock() as mock_client:
            mock_client.add(GET, self.url)

            next(self.subject)

            self.assertEqual(len(mock_client.calls), 1)

    def test_returns_off_if_request_fails(self):
        with RequestsMock() as mock_client:
            mock_client.add(GET, self.url, status=404)

            result = next(self.subject)

            self.assertEqual(result, Status.OFF)

    def test_returns_off_if_request_errors(self):
        with RequestsMock() as mock_client:
            mock_client.add(GET,
                            self.url,
                            body=ConnectionError('Panic! At the Cisco.'))

            with self.assertLogs() as logs:
                result = next(self.subject)

            self.assertEqual(result, Status.OFF)
            self.assertIn('Connection error', logs.output[0])

    def test_logs_error_response(self):
        with RequestsMock() as mock_client:
            mock_client.add(GET, self.url, status=404)

            with self.assertLogs() as logs:
                next(self.subject)

            self.assertIn(f'Received 404 from {self.url!r}', logs.output[0])


class TestWeather(ContractMixin, BaseTestCase):

    def setUp(self):
        super().setUp()
        with mock.patch('sources.weather.getenv') as mock_env:
            mock_env.return_value = 'iamakey'
            self.subject = Weather.from_file(country='GB', city='London')
        self.url = 'http://api.openweathermap.org/data/2.5/forecast'

    def test_sends_query_params(self):
        with RequestsMock() as mock_client:
            mock_client.add(GET, self.url, json={})

            next(self.subject)

            request = mock_client.calls[0].request
            self.assertContainsQuery(request.url, 'id', '2643743')

    def test_sets_api_key(self):
        with RequestsMock() as mock_client:
            mock_client.add(GET, self.url, json={})

            next(self.subject)

            request = mock_client.calls[0].request
            self.assertContainsQuery(request.url, 'APPID', 'iamakey')

    def test_returns_red_if_rain_expected(self):
        response = {
            'list': [
                {'rain': {'3h': 0.0575}},
            ]
        }
        with RequestsMock() as mock_client:
            mock_client.add(GET,
                            self.url,
                            json=response)

            result = next(self.subject)

            self.assertEqual(result, Status.RED)

    def test_returns_green_if_no_rain_expected(self):
        response = {
            'list': [
                {'rain': {}},
            ]
        }
        with RequestsMock() as mock_client:
            mock_client.add(GET, self.url, json=response)

            result = next(self.subject)

            self.assertEqual(result, Status.GREEN)


class TestStockPerformance(ContractMixin, BaseTestCase):

    def setUp(self):
        super().setUp()
        with mock.patch('sources.stock.getenv') as mock_env:
            mock_env.return_value = 'iamakey'
            self.subject = Stock(ticker='PVTL')
        self.url = 'https://www.alphavantage.co/query'

    def test_sends_query_params(self):
        with RequestsMock() as mock_client:
            mock_client.add(GET, self.url, json={})

            next(self.subject)

            request = mock_client.calls[0].request
            self.assertContainsQuery(request.url, 'symbol', 'PVTL')
            self.assertContainsQuery(request.url,
                                     'function',
                                     'TIME_SERIES_DAILY')

    def test_sets_api_key(self):
        with RequestsMock() as mock_client:
            mock_client.add(GET, self.url, json={})

            next(self.subject)

            request = mock_client.calls[0].request
            self.assertContainsQuery(request.url, 'apikey', 'iamakey')

    def test_returns_green_if_up(self):
        with RequestsMock() as mock_client:
            mock_client.add(GET,
                            self.url,
                            json=self._create_json('105.3100', '105.3700'))

            result = next(self.subject)

            self.assertEqual(result, Status.GREEN)

    def test_returns_red_if_down(self):
        with RequestsMock() as mock_client:
            mock_client.add(GET,
                            self.url,
                            json=self._create_json('105.3700', '105.3100'))

            result = next(self.subject)

            self.assertEqual(result, Status.RED)

    def test_returns_off_if_invalid(self):
        with RequestsMock() as mock_client:
            mock_client.add(GET,
                            self.url,
                            json=self._create_json('105.3700', 'foobar'))

            with self.assertLogs() as logs:
                result = next(self.subject)

            self.assertEqual(result, Status.OFF)
            self.assertIn('Parsing error', logs.output[0])

    def test_returns_off_if_null(self):
        with RequestsMock() as mock_client:
            mock_client.add(GET, self.url, json=self._create_json('105.3700'))

            with self.assertLogs() as logs:
                result = next(self.subject)

            self.assertEqual(result, Status.OFF)
            self.assertIn('Parsing error', logs.output[0])

    def test_returns_off_if_missing(self):
        json_response = {
            'Meta Data': {
                '3. Last Refreshed': '2018-07-30 15:41:58',
            },
            'Time Series (Daily)': {
                '2018-07-30': {
                    '1. open': '105.3700',
                }
            }
        }
        with RequestsMock() as mock_client:
            mock_client.add(GET, self.url, json=json_response)

            with self.assertLogs() as logs:
                result = next(self.subject)

            self.assertEqual(result, Status.OFF)
            self.assertIn('Parsing error', logs.output[0])

    @staticmethod
    def _create_json(open_, close_=None):
        return {
            'Meta Data': {
                '3. Last Refreshed': '2018-07-30 15:41:58',
            },
            'Time Series (Daily)': {
                '2018-07-30': {
                    '1. open': open_,
                    '4. close': close_,
                }
            }
        }


class TestSiegeStats(ContractMixin, BaseTestCase):

    def setUp(self):
        super().setUp()
        self.subject = R6Stats(user_id='8ac65307-274a-4246-a241-5d12ed26a462', threshold=0.8)
        self.url = 'https://r6stats.com/stats/8ac65307-274a-4246-a241-5d12ed26a462'

    def test_handles_real_page(self):
        with RequestsMock() as mock_client:
            with open("response.html", "rb") as page:
                mock_client.add(GET, self.url, body=page.read())

            result = next(self.subject)

            self.assertEqual(result, Status.GREEN)

    def test_returns_green_for_ratio_above_threshold(self):
        with RequestsMock() as mock_client:
            mock_client.add(GET, self.url, body=self._create_html('1.1'))

            result = next(self.subject)

            self.assertEqual(result, Status.GREEN)

    def test_returns_green_for_ratio_of_threshold(self):
        with RequestsMock() as mock_client:
            mock_client.add(GET, self.url, body=self._create_html('0.8'))

            result = next(self.subject)

            self.assertEqual(result, Status.GREEN)

    def test_returns_red_for_ratio_below_threshold(self):
        with RequestsMock() as mock_client:
            mock_client.add(GET, self.url, body=self._create_html('0.7'))

            result = next(self.subject)

            self.assertEqual(result, Status.RED)

    def test_returns_off_if_no_value(self):
        with RequestsMock() as mock_client:
            mock_client.add(GET,
                            self.url,
                            body='<div class="title">K/D</div>')

            with self.assertLogs() as logs:
                result = next(self.subject)

            self.assertEqual(result, Status.OFF)
            self.assertIn('Parsing error', logs.output[0])

    def test_returns_off_if_no_title(self):
        with RequestsMock() as mock_client:
            mock_client.add(GET,
                            self.url,
                            body='<div class="value">1.0</div>')

            with self.assertLogs() as logs:
                result = next(self.subject)

            self.assertEqual(result, Status.OFF)
            self.assertIn('Parsing error', logs.output[0])

    def test_returns_off_if_non_float_value(self):
        with RequestsMock() as mock_client:
            mock_client.add(GET,
                            self.url,
                            body=self._create_html('whoops'))

            with self.assertLogs() as logs:
                result = next(self.subject)

            self.assertEqual(result, Status.OFF)
            self.assertIn('Parsing error', logs.output[0])

    @staticmethod
    def _create_html(value):
        return dedent(f'''
        <section class="card stats-card queue-stats-card block block__casual">
            <header class="card__title">
                <h1>Casual Stats</h1>
            </header>
            <div class="card__body">
                <div class="stats-card-item">
                    <div class="stats-card-item__label">K/D Ratio</div>
                    <div class="stats-card-item__value">{value}</div>
                </div>
            </div>
        </section>
        ''')
