statuszero
==========

Scripts for https://thepihut.com/products/status-board-zero

Installation
------------

You need tokens for the following services:

  - `ALPHA_VANTAGE_API_TOKEN`: https://www.alphavantage.co/
  - `OPEN_WEATHER_API_TOKEN`: https://openweathermap.org/api

These will be set in the environment by `statuszero.sh`.

To install this project on a Raspberry Pi and start it when the device boots,
run the following commands:

```bash
# Download the project code
git clone https:gitlab.com/textbook/statuszero.git

# Switch into the project directory
cd statuszero

# Install the requirements for Python 3
pip3 install requirements.txt

# Use an editor of your choice to update statuszero.sh to set the Python
# script location, username and API tokens.

# Copy the start script into the init.d directory
sudo cp statuszero.sh /etc/init.d/

# Switch into the init.d directory
cd /etc/init.d

# Install the start script
sudo update-rc.d statuszero.sh defaults

# Restart the Raspberry Pi
sudo reboot
```

The status lights should come on once the device has rebooted. You can look
at the logs in `<project directory>/statuszero.out` to diagnose any issues.

Known issues
------------

  - If you see issues with `getrandom()` at startup, e.g.:

    ```
    ssl.SSLError: ("bad handshake: Error([('', 'osrandom_rand_bytes', 'getrandom() initialization failed.')],)",)
    ```

    you can try installing `rng-tools` per [these instructions][1].

Development
-----------

Install all of the dependencies as follows:
```sh
$ pip install -r requirements.txt
```

You can lint and test the code using the following commands:
```sh
$ python -m pylint *.py
$ python -m unittest
```

A `Dockerfile` is provided to replicate the GitLab CI environment locally:

```sh
$ docker build . -t statuszero -f Dockerfile.dev
$ docker run statuszero -m pylint *.py
$ docker run statuszero -m unittest
```

  [1]: http://scruss.com/blog/2013/06/07/well-that-was-unexpected-the-raspberry-pis-hardware-random-number-generator/
